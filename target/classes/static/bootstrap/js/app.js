$("#loginbtn").click(()=>{

   let login = $("#login").val();
   let password = $("#password").val();
   $.ajax({
     method: "POST",
     url: "/user/logon",
     dataType: "json",
     data: { login: login , password: password }
   })
       .done(function( ret, textStatus, jqXHR ) {
             if(textStatus === "400" )
             {
              alert( ret.data.message );
             }
             else
             {
             console.log(ret.data.url)
             document.location.href = ret.data.url;
             }
           })
            .fail(function( ret, textStatus, errorThrown ) {
                   let json = ret.responseJSON;
                   console.log(ret.responseJSON);
                   alert(json.data.message);

            })
});


function destroy(id){
  $.ajax({
      method: "DELETE",
      url: "/item/delete",
      dataType: "json",
      data: { id: id}
    })
      .done(function( ret, textStatus, jqXHR ) {
        if(textStatus === "400" )
        {
         alert( ret.data.message );
        }
        else
        {
        console.log(ret.data.url)
        document.location.href = ret.data.url;
        }
      })
       .fail(function( ret, textStatus, errorThrown ) {
              if(textStatus === "400" )
              {
               alert( ret.data.message );
              }
              else
              {
              console.log(ret.data.url)
              document.location.href = ret.data.url;
              }
            })

}

function check(id, cmp){

    let isChecked = cmp.checked
    $.ajax({
          method: "PUT",
          url: "/item/update",
          dataType: "json",
          data: { id: id, check: isChecked}
        })
          .done(function( ret ) {
            console.log(ret.data.url)
            document.location.href = ret.data.url;
          });
}

function destroyTask(id){
  $.ajax({
      method: "DELETE",
      url: "/task/delete",
      dataType: "json",
      data: { id: id}
    })
      .done(function( ret ) {
        console.log(ret.data.url)
        document.location.href = ret.data.url;
      });
}