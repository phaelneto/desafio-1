package com.stefanini.desafio1.model;

import javax.persistence.*;

@Entity
public class Item {

    private Integer idItem;
    private String description;
    private Task task;
    private Boolean done;

    public Item() {
    }

    @Id
    @Column(name = "id_item")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne
    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Column(name = "done")
    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
