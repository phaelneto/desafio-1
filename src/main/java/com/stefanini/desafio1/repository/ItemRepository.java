package com.stefanini.desafio1.repository;

import com.stefanini.desafio1.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
}
