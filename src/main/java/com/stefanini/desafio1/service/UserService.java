package com.stefanini.desafio1.service;

import com.stefanini.desafio1.dto.LoginDTO;
import com.stefanini.desafio1.model.User;
import com.stefanini.desafio1.repository.UserRepository;
import com.stefanini.desafio1.util.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public void addUser(User user)
    {
        user.setPassword(PasswordUtils.gerarHashSHA256(user.getPassword()));
        userRepository.save(user);
    }

    public List<User> listUser()
    {
        return userRepository.findAll();
    }

    public boolean isValidUser(LoginDTO loginDTO)
    {
        return !userRepository.findByEmailAndPassword(loginDTO.getLogin(), PasswordUtils.gerarHashSHA256(loginDTO.getPassword())).isEmpty();
    }

    public Optional<User> findByEmail(String email)
    {
        return userRepository.findByEmail(email);
    }
}
