package com.stefanini.desafio1.service;

import com.stefanini.desafio1.model.Item;
import com.stefanini.desafio1.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    public void add(Item item)
    {
        itemRepository.save(item);
    }

    public Optional<Item> getById(Integer id)
    {
        return itemRepository.findById(id);
    }

    public void delete(Integer id)
    {
        itemRepository.deleteById(id);
    }

    public void deleteAll()
    {
        itemRepository.deleteAll();
    }

}
