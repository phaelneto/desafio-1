package com.stefanini.desafio1.service;


import com.stefanini.desafio1.model.Task;
import com.stefanini.desafio1.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    public void add(Task task)
    {
        taskRepository.save(task);
    }

    public List<Task> list()
    {
        return taskRepository.findAll();
    }

    public Optional<Task> getById(Integer id)
    {
        return taskRepository.findById(id);
    }

    public void delete(Integer id)
    {
        taskRepository.deleteById(id);
    }
}
