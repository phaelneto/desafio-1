package com.stefanini.desafio1.controller;

import com.stefanini.desafio1.dto.LoginDTO;
import com.stefanini.desafio1.dto.ReturnClientDTO;
import com.stefanini.desafio1.model.User;
import com.stefanini.desafio1.service.UserService;
import com.stefanini.desafio1.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/user/signup")
    public ModelAndView signup()
    {
        User user = new User();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user/create");
        modelAndView.addObject(user);
       return modelAndView;
    }

    @PostMapping("/user/store")
    public String store(@Valid @ModelAttribute final User user, final BindingResult bindingResult, RedirectAttributes redirectAttribute)
    {
        if(bindingResult.hasErrors())
        {
            return "user/create";
        }
        userService.addUser(user);
        redirectAttribute.addFlashAttribute("message", "Atualizado com sucesso");
        return "index";
    }


    @GetMapping("/user")
    public ModelAndView index()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user/index");
        modelAndView.addObject("allUser" , userService.listUser());
        return modelAndView;
    }

    @GetMapping("/user/login")
    public String login()
    {
        return "login";
    }

    @PostMapping("/user/logon")
    public ResponseEntity<Response<ReturnClientDTO>> logon(@Valid @ModelAttribute final LoginDTO loginDTO, final BindingResult bindingResult, RedirectAttributes redirectAttribute, HttpSession httpSession) throws Exception {

        Response<ReturnClientDTO> response = new Response<>();
        ReturnClientDTO returnClientDTO = new ReturnClientDTO();
        returnClientDTO.setUrl("/task/");
        returnClientDTO.setMessage("Processado com sucesso");
        try
        {
            String erros;
            if(bindingResult.hasErrors())
            {
               erros = bindingResult.getAllErrors().stream().map(error -> error.getDefaultMessage()).collect(Collectors.joining(" / "));
               throw new Exception(erros);
            }

            if(!userService.isValidUser(loginDTO))
            {
                throw new Exception("Usuário e/ou senha invalida");
            }

            ///setar sessão
            httpSession.setAttribute("user",  loginDTO.getLogin());
            httpSession.setAttribute("authenticate", true);

            response.setData(returnClientDTO);
            return ResponseEntity.ok(response);
        }
        catch (Exception e)
        {
            returnClientDTO.setMessage("Problemas: " + e.getMessage());
            response.setData(returnClientDTO);
            return ResponseEntity.badRequest().body(response);
        }


    }

    @GetMapping("/user/logoff")
    public ModelAndView logoff(HttpSession httpSession)
    {
        httpSession.removeAttribute("user");
        httpSession.removeAttribute("authenticate");
        User user = new User();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject(user);
        return modelAndView;
    }
}
