package com.stefanini.desafio1.controller;

import com.stefanini.desafio1.dto.ReturnClientDTO;
import com.stefanini.desafio1.model.Item;
import com.stefanini.desafio1.model.Task;
import com.stefanini.desafio1.model.User;
import com.stefanini.desafio1.service.ItemService;
import com.stefanini.desafio1.service.TaskService;
import com.stefanini.desafio1.service.UserService;
import com.stefanini.desafio1.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    UserService userService;

    @Autowired
    ItemService itemService;

    @GetMapping("/")
    public ModelAndView getIndex()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping("/task/create")
    public ModelAndView create()
    {
        Task task = new Task();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task/create");
        modelAndView.addObject(task);
        return modelAndView;
    }

    @PostMapping("/task/store")
    public ModelAndView store(@Valid @ModelAttribute final Task task, final BindingResult bindingResult, RedirectAttributes redirectAttribute, HttpSession httpSession)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(bindingResult.hasErrors())
        {
            modelAndView.setViewName("task/create");
            return modelAndView;
        }

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<User> user = userService.findByEmail(httpSession.getAttribute("user").toString());
        task.setUser(user.get());
        taskService.add(task);
        modelAndView.addObject("message", "Tarefa adicionada com Sucesso");
        modelAndView.addObject("allTasks", user.get().getTasks());
        modelAndView.setViewName("task/index");
        return modelAndView;
    }

    @GetMapping("/task")
    public ModelAndView index(HttpSession httpSession)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<User> user = userService.findByEmail(httpSession.getAttribute("user").toString());

        List<Task> list = user.get().getTasks();

        if(list == null)
            list = new ArrayList<>();

        modelAndView.setViewName("task/index");
        modelAndView.addObject("allTasks" , list);
        return modelAndView;
    }

    @DeleteMapping("/task/delete")
    public ResponseEntity<Response<ReturnClientDTO>> delete(@RequestParam Integer id, HttpSession httpSession)
    {
        Task task = taskService.getById(id).get();
        Response<ReturnClientDTO> response = new Response<>();
        ReturnClientDTO returnClientDTO = new ReturnClientDTO();
        returnClientDTO.setUrl("/task/");
        returnClientDTO.setMessage("Processado com sucesso");

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }

        List<Item> list =  task.getItems();
        if(!list.isEmpty())
        {
            itemService.deleteAll();
        }
        taskService.delete(id);
        response.setData(returnClientDTO);
        return ResponseEntity.ok(response);
    }

}
