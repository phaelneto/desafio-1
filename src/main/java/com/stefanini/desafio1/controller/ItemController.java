package com.stefanini.desafio1.controller;

import com.stefanini.desafio1.dto.ReturnClientDTO;
import com.stefanini.desafio1.model.Item;
import com.stefanini.desafio1.model.Task;
import com.stefanini.desafio1.model.User;
import com.stefanini.desafio1.repository.ItemRepository;
import com.stefanini.desafio1.service.ItemService;
import com.stefanini.desafio1.service.TaskService;
import com.stefanini.desafio1.service.UserService;
import com.stefanini.desafio1.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
public class ItemController {

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @Autowired
    ItemService itemService;

    @GetMapping("/item/{id}")
    public ModelAndView index(HttpSession httpSession, @PathVariable Integer id)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<User> user = userService.findByEmail(httpSession.getAttribute("user").toString());

        Optional<Task> task = taskService.getById(id);

        if(task.isEmpty())
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }
        modelAndView.addObject("task", task.get());
        modelAndView.addObject("allItems", task.get().getItems());
        modelAndView.setViewName("/item/index");
        return modelAndView;
    }

    @GetMapping("/item/create/{id}")
    public ModelAndView create(@PathVariable Integer id, HttpSession httpSession)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<Task> task = taskService.getById(id);

        if(task.isEmpty())
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }
        Item item = new Item();
        modelAndView.setViewName("item/create");
        modelAndView.addObject("task", task.get());
        modelAndView.addObject(item);
        return modelAndView;
    }

    @PostMapping("/item/store/{id}")
    public ModelAndView store(@Valid @ModelAttribute final Item item, @PathVariable Integer id, final BindingResult bindingResult, HttpSession httpSession)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(bindingResult.hasErrors())
        {
            modelAndView.setViewName("item/create");
            return modelAndView;
        }

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<Task> task = taskService.getById(id);

        if(task.isEmpty())
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }
        item.setTask(task.get());

        itemService.add(item);
        modelAndView.addObject("allItems", task.get().getItems());
        modelAndView.addObject("task", task.get());
        modelAndView.addObject("message", "Item da Tarefa adicionada com Sucesso");
        modelAndView.setViewName("item/index");
        return modelAndView;
    }

    @GetMapping("/item/alter/{id}")
    public ModelAndView alter(@PathVariable Integer id, HttpSession httpSession)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<Item> item = itemService.getById(id);

        if(item.isEmpty())
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }
        modelAndView.setViewName("item/alter");
        modelAndView.addObject("task", item.get().getTask());
        modelAndView.addObject("item", item.get());
        return modelAndView;
    }


    @PostMapping("/item/update/{id}")
    public ModelAndView update(@Valid @ModelAttribute final Item item, @PathVariable Integer id, final BindingResult bindingResult, HttpSession httpSession)
    {
        ModelAndView modelAndView = new ModelAndView();

        if(bindingResult.hasErrors())
        {
            modelAndView.setViewName("item/create");
            return modelAndView;
        }

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }

        Optional<Task> task = taskService.getById(id);

        if(task.isEmpty())
        {
            modelAndView.setViewName("index");
            return modelAndView;
        }
        System.out.println(item.getTask());

        item.setTask(task.get());

        itemService.add(item);
        modelAndView.addObject("allItems", task.get().getItems());
        modelAndView.addObject("task", task.get());
        modelAndView.addObject("message", "Item da Tarefa adicionada com Sucesso");
        modelAndView.setViewName("item/index");
        return modelAndView;
    }


    @DeleteMapping("/item/delete")
    public ResponseEntity<Response<ReturnClientDTO>> delete(@RequestParam Integer id, HttpSession httpSession)
    {
        Task task = itemService.getById(id).get().getTask();
        Response<ReturnClientDTO> response = new Response<>();
        ReturnClientDTO returnClientDTO = new ReturnClientDTO();
        returnClientDTO.setUrl("/item/" + task.getIdTask());
        returnClientDTO.setMessage("Processado com sucesso");

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }

        itemService.delete(id);
        response.setData(returnClientDTO);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/item/update")
    public ResponseEntity<Response<ReturnClientDTO>> updateJson(@RequestParam Integer id, @RequestParam boolean check,HttpSession httpSession)
    {

        Item item = itemService.getById(id).get();
        Task task = item.getTask();
        Response<ReturnClientDTO> response = new Response<>();
        ReturnClientDTO returnClientDTO = new ReturnClientDTO();
        returnClientDTO.setUrl("/item/" + task.getIdTask());
        returnClientDTO.setMessage("Processado com sucesso");

        if(httpSession.getAttribute("user") == null || (httpSession.getAttribute("authenticate") != null && !(boolean) httpSession.getAttribute("authenticate")))
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
        item.setDone(check);
        itemService.add(item);
        response.setData(returnClientDTO);
        return ResponseEntity.ok(response);
    }

}
