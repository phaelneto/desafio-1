package com.stefanini.desafio1.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class LoginDTO {

    private String login;
    private String password;

    @Email
    @NotBlank(message = "Email deve ser informado")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @NotBlank(message = "Senha deve ser informada")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
